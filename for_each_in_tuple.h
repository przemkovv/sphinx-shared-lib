#pragma once

#include <tuple>
#include <type_traits>


namespace Sphinx::Utils {

template <auto I = 0, typename Func, typename... Ts>
typename std::enable_if<I == sizeof...(Ts)>::type
for_each_in_tuple(std::tuple<Ts...> &, Func)
{
}

template <auto I = 0, typename Func, typename... Ts>
    typename std::enable_if <
    I<sizeof...(Ts)>::type for_each_in_tuple(std::tuple<Ts...> &tpl, Func func)
{
  func(std::get<I>(tpl));
  for_each_in_tuple<I + 1>(tpl, func);
}

} // namespace Sphinx::Utils
