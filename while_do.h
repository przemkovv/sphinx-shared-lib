#pragma once

#include <chrono>   // for milliseconds
#include <future>   // for future_status, async, future_status...

namespace Sphinx::Utils {

template <typename Task1, typename Task2>
auto while_do(const Task1 &task_while, const Task2 &task_do)
{

  auto future = std::async(std::launch::async, task_while);
  std::future_status status;
  do {
    task_do();
    status = future.wait_for(std::chrono::milliseconds(5));
  } while (status != std::future_status::ready);

  task_do();

  return future.get();
}

} // namespace Sphinx::Utils
