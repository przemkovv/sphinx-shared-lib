
#pragma once

#include <memory>          // for shared_ptr
#include <spdlog/spdlog.h> // for level_enum
#include <string>          // for string

namespace Sphinx {

using Logger = std::shared_ptr<spdlog::logger>;

Logger make_logger(const std::string &name);

Logger make_logger(const std::string &name,
                   const spdlog::level::level_enum &level);

static Logger global_logger = make_logger("Sphinx::GLOBAL");

} // namespace Sphinx
