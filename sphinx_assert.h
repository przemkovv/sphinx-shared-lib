
#pragma once

#include <type_traits>

template <typename T>
struct assert_false : std::false_type {
};

template <typename T>
void static_not_implemented_yet()
{
  static_assert(assert_false<T>::value, "Not implemented yet");
}

#define NOT_IMPLEMENTED_YET() SPHINX_ASSERT(false, "Not implemented yet.")

#ifndef SPHINX_NDEBUG

#include "logger.h"
#include <exception>
#include <string>

/// Exception type for assertion failures
class AssertionFailureException : public std::exception {
private:
  const char *expression_;
  const char *file_;
  int line_;
  std::string message_;
  std::string report;
  Sphinx::Logger logger_;

public:
  /// Log error before throwing
  void LogError();

  /// Construct an assertion failure exception
  AssertionFailureException(const char *expression,
                            const char *file,
                            int line,
                            const std::string &message);

  AssertionFailureException(const AssertionFailureException &) = default;
  AssertionFailureException &
  operator=(const AssertionFailureException &) = default;

  /// The assertion message
  virtual const char *what() const noexcept(true);

  /// The expression which was asserted to be true
  const char *Expression() const;

  /// Source file
  const char *File() const;

  /// Source line
  int Line() const;

  /// Description of failure
  const char *Message() const;
};

/// Assert that EXPRESSION evaluates to true, otherwise raise
/// AssertionFailureException with associated MESSAGE (which may use C++
/// stream-style message formatting)
#define SPHINX_ASSERT(EXPRESSION, ...)                                         \
  if (!(EXPRESSION)) {                                                         \
    throw AssertionFailureException(#EXPRESSION, __FILE__, __LINE__,           \
                                    (fmt::format(__VA_ARGS__)));               \
  }

#else

#define SPHINX_ASSERT(EXPRESSION, ...)                                         \
  {                                                                            \
  }

#endif // SPHINX_NDEBUG
