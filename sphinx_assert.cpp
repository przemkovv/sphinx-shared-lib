
#include "logger.h"
#include "sphinx_assert.h"
#include <fmt/format.h>    // for MemoryWriter, format, BasicWriter
#include <memory>          // for __shared_ptr_access
#include <spdlog/spdlog.h> // for logger

#ifndef SPHINX_NDEBUG
////----------------------------------------------------------------------
void AssertionFailureException::LogError()
{
  logger_->error(report);
}

////----------------------------------------------------------------------
AssertionFailureException::AssertionFailureException(const char *expression,
                                                     const char *file,
                                                     int line,
                                                     const std::string &message)
  : expression_(expression),
    file_(file),
    line_(line),
    message_(message),
    report(),
    logger_(Sphinx::make_logger("Assert"))
{
  fmt::MemoryWriter outputStream;

  if (!message.empty()) {
    outputStream << fmt::format("{}: ", message);
  }

  std::string expressionString = expression;
  if (expressionString == "false" || expressionString == "0" ||
      expressionString == "FALSE") {
    outputStream << "Unreachable code assertion";
  }
  else {
    outputStream << fmt::format("Assertion '{}'", expression);
  }

  outputStream << fmt::format(" failed in file '{}' line {}", file, line);
  report = outputStream.str();

  LogError();
}
//----------------------------------------------------------------------
const char *AssertionFailureException::what() const noexcept(true)
{
  return report.c_str();
}

//----------------------------------------------------------------------
const char *AssertionFailureException::Expression() const
{
  return expression_;
}

//----------------------------------------------------------------------
const char *AssertionFailureException::File() const
{
  return file_;
}

//----------------------------------------------------------------------
int AssertionFailureException::Line() const
{
  return line_;
}

//----------------------------------------------------------------------
const char *AssertionFailureException::Message() const
{
  return message_.c_str();
}

#endif // SPHINX_NDEBUG
